package com.zhou.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * (Students)实体类
 *
 * @author makejava
 * @since 2020-05-31 17:04:20
 */
@TableName("students")
public class Students implements Serializable {
    private static final long serialVersionUID = 424649071203146283L;
    
    private Integer id;
    
    private String name;

    @TableField("class_id")
    private Integer classId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

}