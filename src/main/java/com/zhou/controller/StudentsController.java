package com.zhou.controller;

import com.zhou.entity.Students;
import com.zhou.service.StudentsService;
import com.zhou.util.Result;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Students)表控制层
 *
 * @author makejava
 * @since 2020-05-31 17:04:22
 */
@ApiOperation("学生层")
@RestController
@RequestMapping("students")
public class StudentsController {
    /**
     * 服务对象
     */
    @Resource
    private StudentsService studentsService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public Logger logger = LogManager.getLogger(StudentsController.class);
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiOperation("查询单值")
    @GetMapping("selectOne")
    public Result selectOne(Integer id) {
        if(this.studentsService.queryById(id)!=null){
            logger.info("单值查询");
            return Result.ok().data("student",this.studentsService.queryById(id)) ;
        }else{

            logger.info("单值查询");
            return Result.no().data("student",null);
        }

    }

    @ApiOperation("查询全部")
    @GetMapping("findAll")
    public Result findAll(){

        if(studentsService.findAll()!=null){
            logger.info("查询全部");
            List<Students> list =studentsService.findAll();
            stringRedisTemplate.opsForValue().set("a","第一个");
            return Result.ok().data("list",list);
        }
        else{
            logger.info("查询全部");
            return Result.ok().data("list",null);
        }
    }
}