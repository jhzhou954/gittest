package com.zhou.util;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author com.hr.Text
 * @date 2020/4/3 - 18:48
 */

@Configuration
@EnableSwagger2
public class Swagger2 {
//    @Bean
//    public Docket createRestApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .apiInfo(apiInfo())
//                .select()
//                .apis(RequestHandlerSelectors.basePackage
//                        ("com.zhou.controller")//此出换为自己的controller路径
//                )
//                .paths(PathSelectors.any()).build();
//    }
    @Bean
    public Docket webApiConfig() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("webApi")
                .apiInfo(webApiInfo())
                .select()
                .paths(Predicates.not(PathSelectors.regex("/admin/.*")))
                .paths(Predicates.not(PathSelectors.regex("/error.*")))
                .build();
    }

    private ApiInfo webApiInfo(){

        return new ApiInfoBuilder()

                .title("周俊豪")

                .description("swagger测试")

                .version("1.0")

                .contact(new Contact("JAVA", "http://www.baidu.com", "jhzhou954@163.com"))

                .build();

    }
//    private ApiInfo apiInfo() {
//        return new ApiInfoBuilder()
//                .title("Springboot-api文档")
//                .description("")
//                .termsOfServiceUrl("")
//                .version("1.0")
//                .build();
//    }
}