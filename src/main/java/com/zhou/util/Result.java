package com.zhou.util;

import com.zhou.entity.CodeConfig;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Result {
    private Boolean success;
    private  Integer code;
    private String message;
    private Map <String,Object> data=new HashMap<String, Object>();
    //构造方法私有别人不能nwe
    private Result(){}

    //成功
    public static Result ok(){
        Result rs=new Result();
        rs.setSuccess(true);
        rs.setCode(CodeConfig.success );
        rs.setMessage("成功");
        return rs;
    }
    //失败
    public static Result no(){
        Result rs=new Result();
        rs.setSuccess(false);
        rs.setCode(CodeConfig.error );
        rs.setMessage("失败");
        return rs;
    }
    public Result success(Boolean success){
        this.setSuccess(success);
        return this;
    }
    public Result code(Integer code){
        this.setCode(code);
        return this;
    }
    public Result message(String message){
        this.setMessage(message);
        return this;
    }
    public Result data(String key, Object value){
        this.data.put(key, value);
        return this;
    }

    public Result data(Map<String, Object> map){
        this.setData(map);
        return this;
    }

}
